import axios from "axios";

const loginAdmin = (email, password) =>
  new Promise((resolve, reject) => {
    axios.post(`${process.env.REACT_APP_ROOT_URL}/apis/login/player`, {
      email,
      password,
    });
  });

export default loginAdmin;
