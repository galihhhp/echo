import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import "App.scss";
import LoginAdmin from "pages/LoginAdmin";
import LoginUser from "pages/LoginUser";
import HomePage from "pages/Home";
import Game from "pages/Game";
import Register from "pages/Register";
import Dashboard from "pages/Dashboard";
import ROUTES from "configs/routes";
import PrivateRoute from "Components/Common/PrivateRoute";

export default function App() {
  return (
    <div className="App">
      <Router>
        <Switch>
          <Route exact path={ROUTES.HOMEPAGE} component={HomePage} />
          <Route exact path={ROUTES.LOGIN_ADMIN} component={LoginAdmin} />
          <Route exact path={ROUTES.LOGIN_USER} component={LoginUser} />
          <Route exact path={ROUTES.REGISTER} component={Register} />
          <Route exact path={ROUTES.GAME} component={Game} />
          <PrivateRoute path={ROUTES.DASHBOARD} exact>
            <Dashboard />
          </PrivateRoute>
        </Switch>
      </Router>
    </div>
  );
}
