const ROUTES = {
  HOMEPAGE: "/",
  LOGIN_ADMIN: "/admin",
  LOGIN_USER: "/user",
  REGISTER: "/register",
  GAME: "/game",
  DASHBOARD: "dasboard",
};

export default ROUTES;
