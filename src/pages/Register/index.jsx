import React, { useEffect, useState } from 'react';
import FormInputComponent from '../../Components/Common/FormInput';
import ButtonComponent from '../../Components/Common/ButtonComponent';

export default function Register() {
  const [data, setData] = useState({
    username: '',
    password: '',
    confirmPassword: '',
    email: '',
    fullName: '',
  });

  useEffect(() => {
    document.title = 'Register - Echo';
  });

  const handleEventChange = (event) => {
    console.log({ ...data });
    setData({ ...data, [event.target.name]: event.target.value });
  };

  const handleRegister = () => {
    const show = document.getElementById('show');
    show.innerHTML = `${data.username} ${data.password} ${data.confirmPassword} ${data.email} ${data.fullName}`;
  };

  const {
    username, password, confirmPassword, email, fullName,
  } = data;

  return (
    <div className="container-fluid d-flex flex-column justify-content-center align-items-center">
      <h1 className="form-title">Register</h1>
      <div className="form-wrapper">
        <div className="mb-3">
          <FormInputComponent
            handleEventChange={handleEventChange}
            type="text"
            name="username"
            id="username"
            placeholder="Your Username"
            value={username}
          />
        </div>
        <div className="mb-3">
          <FormInputComponent
            handleEventChange={handleEventChange}
            type="password"
            name="password"
            id="password"
            placeholder="Your Password"
            value={password}
          />
        </div>
        <div className="mb-3">
          <FormInputComponent
            handleEventChange={handleEventChange}
            type="password"
            name="confirmPassword"
            id="confirmPassword"
            placeholder="Confirm Your Password"
            value={confirmPassword}
          />
          <div className="mb-3 mt-3">
            <FormInputComponent
              handleEventChange={handleEventChange}
              type="email"
              name="email"
              id="email"
              placeholder="Your Email"
              value={email}
            />
          </div>
          <div className="mb-3">
            <FormInputComponent
              handleEventChange={handleEventChange}
              type="text"
              name="fullName"
              id="fullName"
              placeholder="Your Full Name"
              value={fullName}
            />
          </div>
        </div>
        <div id="show" />
        <div className="pb-2 text-danger">
          {password === confirmPassword ? null : 'Your Password Not Match'}
        </div>
        <ButtonComponent buttonHandler={handleRegister} text="Sign Up" />
      </div>
    </div>
  );
}
