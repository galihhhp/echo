import React from "react";
import { render } from "@testing-library/react";
import Register from "..";

test("Register renders correctly", () => {
  const { container } = render(<Register />);
  expect(container).toMatchSnapshot();
});
