import React from "react";
import { render } from "@testing-library/react";
import LoginAdmin from "..";

test("Login Admin renders correctly", () => {
  const { container } = render(<LoginAdmin />);
  expect(container).toMatchSnapshot();
});
