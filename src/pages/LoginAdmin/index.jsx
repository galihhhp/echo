import React, { useEffect, useState } from "react";
import axios from "axios";
import FormInputComponent from "../../Components/Common/FormInput";
import ButtonComponent from "../../Components/Common/ButtonComponent";

export default function LoginAdmin() {
  const [data, setData] = useState({
    username: "",
    password: "",
  });

  useEffect(() => {
    document.title = "Login - Echo";
  });

  //! GET API FROM /users

  // const getUsersApi = () => {
  //   axios
  //     .get('http://localhost:3000/apis/users', users)
  //     .then((res) => console.log(res))
  //     .catch((err) => console.log(err));
  // };

  // useEffect(() => {
  //   getUsersApi();
  // }, []);

  const handleLogin = () => {
    axios
      .post("http://localhost:3000/apis/login/admin", data)
      .then((res) => console.log(res.data));
  };

  const handleEventChange = (event) => {
    setData({ ...data, [event.target.name]: event.target.value });
  };

  const { username, password } = data;

  return (
    <div className="container-fluid d-flex flex-column justify-content-center align-items-center">
      <h1 className="form-title">Login</h1>
      <div className="form-wrapper">
        <div className="mb-3">
          <FormInputComponent
            handleEventChange={handleEventChange}
            type="text"
            name="username"
            id="username"
            placeholder="Your Username"
            value={username}
          />
        </div>
        <div className="mb-3">
          <FormInputComponent
            handleEventChange={handleEventChange}
            type="password"
            name="password"
            id="password"
            placeholder="Your Password"
            value={password}
          />
        </div>
        <ButtonComponent buttonHandler={handleLogin} text="Sign In" />
      </div>
    </div>
  );
}
