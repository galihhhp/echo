import React from "react";
import NavbarComponent from "../../Components/Common/NavbarComponent";

const Dashboard = () => (
  <>
    <NavbarComponent />
    <div className="container-flex d-flex flex-column justify-content-center align-items-center">
      <table className="table">
        <thead>
          <tr className="text-center">
            <th scope="col">ID</th>
            <th scope="col">USERNAME</th>
            <th scope="col">PASSWORD</th>
            <th scope="col">EMAIL</th>
            <th scope="col">FULL NAME</th>
            <th scope="col">DELETE</th>
          </tr>
        </thead>
        <tbody id="listUsers" className="tables" />
      </table>
    </div>
    <form action="http://localhost:3001/createUser">
      <button className="create-btn btn mt-5" type="button">
        CREATE
      </button>
    </form>
    <form action="http://localhost:3001/history">
      <button id="toAllHistory" className="history-btn btn mt-3" type="button">
        HISTORY
      </button>
    </form>
    <form action="http://localhost:3001/admin">
      <button id="addAdmin" className="admin-btn btn mt-3" type="button">
        ADMIN
      </button>
    </form>
    <button id="reset" className="reset-btn btn mt-3 mb-5" type="button">
      RESET DB
    </button>
  </>
);

export default Dashboard;
