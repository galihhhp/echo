import React from 'react';
import NavbarComponent from '../../Components/Common/NavbarComponent';

const HomePage = () => (
  <>
    <NavbarComponent />
    <div className="home-wrapper">
      <div className="container-fluid">
        <div className="d-flex flex-column justify-content-center home-content">
          <h1>
            Rock,
            <br />
            Scissor, and Paper
          </h1>
          <p>You can play the game with your friend and the computer</p>
        </div>
        <button className="home-btn__login mr-3" type="button">
          Login
        </button>
        <button className="home-btn__register" type="button">
          Register
        </button>
      </div>
    </div>
  </>
);

export default HomePage;
