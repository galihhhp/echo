import React from "react";
import { render } from "@testing-library/react";
import Game from "..";

test("Game renders correctly", () => {
  const { container } = render(<Game />);
  expect(container).toMatchSnapshot();
});
