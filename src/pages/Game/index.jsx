import React, { useEffect } from 'react';
// import logo from '../../assets/img/logo.png';
import rock from '../../assets/img/rock.png';
import paper from '../../assets/img/paper.png';
import scissor from '../../assets/img/scissor.png';
import refresh from '../../assets/img/refresh.png';

export default function Game() {
  useEffect(() => {
    document.title = 'Game - Echo';
  });

  return (
    <>
      {/* <header className="container-fluid d-flex mt-2">
        <h3 className="back-btn">&lt;</h3>
        <img src={logo} alt="" className="logo" />
        <h3 className="game-title text-uppercase">rock paper scissors</h3>
      </header> */}

      <main className="container mar-cus game-background">
        <article className="row mar-cus">
          <div className="col">
            <h2 className="text-uppercase text-center">player</h2>
          </div>
          <div className="col" />
          <div className="col">
            <h2 className="text-uppercase text-center">com</h2>
          </div>
        </article>
        <article className="row mar-cus">
          <div className="col">
            <p className="text-center">
              <input
                type="image"
                src={rock}
                width="60px"
                className="player rock"
                id="rockPlayer"
                alt="rock"
              />
            </p>
          </div>
          <div className="col" />
          <div className="col">
            <p className="rock text-center">
              <img
                className="computer compRock"
                src={rock}
                alt="rock"
                id="Rock"
                title="rock"
              />
            </p>
          </div>
        </article>
        <article className="row mar-cus">
          <div className="col">
            <p className="text-center">
              <input
                type="image"
                src={paper}
                width="60px"
                className="player paper"
                alt="paper"
                id="paperPlayer"
              />
            </p>
          </div>
          <div className="col">
            <h1 id="gameResult" className="text-center game-result displaySize">
              vs
            </h1>
          </div>
          <div className="col">
            <p className="paper text-center">
              <img className="computer compPaper" src={paper} alt="paper" />
            </p>
          </div>
        </article>

        <article className="row mar-cus">
          <div className="col">
            <p className="text-center">
              <input
                type="image"
                src={scissor}
                width="80px"
                className="player scissor"
                alt="scissor"
                id="scissorPlayer"
              />
            </p>
          </div>
          <div className="col" />
          <div className="col">
            <p className="scissor text-center">
              <img
                className="computer compScissor"
                src={scissor}
                alt="scissor"
              />
            </p>
          </div>
        </article>

        <article className="row mar-cus ">
          <p className="text-center mar-aut">
            <img className="refresh" src={refresh} alt="refresh" />
          </p>
        </article>
      </main>
    </>
  );
}
