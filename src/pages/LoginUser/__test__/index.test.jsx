import React from "react";
import { render } from "@testing-library/react";
import LoginUser from "..";

test("Login User renders correctly", () => {
  const { container } = render(<LoginUser />);
  expect(container).toMatchSnapshot();
});
