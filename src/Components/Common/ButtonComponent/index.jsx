/* eslint-disable react/prop-types */
import React from 'react';

export default function ButtonComponent({ text, buttonHandler }) {
  return (
    <>
      <button
        onClick={() => buttonHandler()}
        className="form-button__login"
        type="submit"
      >
        {text}
      </button>
    </>
  );
}
