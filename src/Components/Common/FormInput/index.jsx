/* eslint-disable react/prop-types */
import React from 'react';

const FormInputComponent = ({
  handleEventChange,
  name,
  type,
  id,
  placeholder,
  value,
}) => (
  <>
    <input
      onChange={handleEventChange}
      name={name}
      type={type}
      className="form-control"
      id={id}
      placeholder={placeholder}
      value={value}
      required
    />
  </>
);

export default FormInputComponent;
