import React from "react";
import ROUTES from "configs/routes";
import { Redirect } from "react-router-dom";

const PrivateRoute = ({ children }) => {
  if (!localStorage.getItem("token")) return <Redirect to={ROUTES.HOMEPAGE} />;

  return children;
};

export default PrivateRoute;
