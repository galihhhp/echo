import React from 'react';

export default function NavbarComponent() {
  return (
    <nav className="navbar">
      <div className="container-fluid">
        <h1 className="navbar-brand">Game</h1>
        <form className="d-flex">
          <button className="btn admin-btn" type="submit">
            Admin
          </button>
        </form>
      </div>
    </nav>
  );
}
